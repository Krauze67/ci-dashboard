package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
	"time"

	yaml "gopkg.in/yaml.v2"
)

var cmd Command

type config struct {
	Repos []string `yaml:"repo"`
}

type outJSON struct {
	Repos []outRepoJSON `json:"repo"`
}

type outRepoJSON struct {
	Name     string `json:"name"`
	Terminal string `json:"terminal"`
	Status   string `json:"status"`
}

const (
	// SUCCESS if all Ok
	SUCCESS = "success"
	// FAILED if some error occurs
	FAILED = "failed"
)

func main() {
	isReportOnly := flag.Bool("report-only", false, "generate report only")
	flag.Parse()

	if !(*isReportOnly) {
		mainRun()
	}
	mainDashboard()
}

func mainRun() {
	// Declare var/const
	cmd.Verbose(false)
	var outRes outJSON
	outRes.Repos = make([]outRepoJSON, 0)

	// Getting Yaml
	var conf config

	data, err := ioutil.ReadFile("../config.yaml")
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	err = yaml.Unmarshal([]byte(data), &conf)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	// Pull(fetch+rebase), Get, Test, Build, and Lint
	cmd.Verbose(false)
	gopathDir, _ := cmd.Run("go", "env", "GOPATH")
	gopathDir = strings.Trim(gopathDir, "\n\r\t ")

	for _, repo := range conf.Repos {
		outRepo := outRepoJSON{Name: repo, Status: SUCCESS}

		cmd.Verbose(false)
		pathparts := strings.Split(repo, "/")
		if len(pathparts) <= 1 {
			pathparts = strings.Split(repo, "\\")
		}

		repodir := filepath.Join(append([]string{gopathDir, "src"}, pathparts...)...)
		out, err := cmd.RunInDir(repodir, "git", "pull", "-v", "--no-log", "--all")
		outRepo.Terminal += out
		if err != nil {
			outRepo.Status = FAILED
			outRes.Repos = append(outRes.Repos, outRepo)
			log.Printf("error: %v", err)
			continue
		}

		out, _ = cmd.Run("go", "get", "-v", repo)
		outRepo.Terminal += out

		cmd.Verbose(false)
		packsStr, err := cmd.Run("go", "list", repo+"/...")
		outRepo.Terminal += packsStr
		if err != nil {
			outRepo.Status = FAILED
			outRes.Repos = append(outRes.Repos, outRepo)
			log.Printf("error: %v", err)
			continue
		}

		if !strings.Contains(packsStr, "\" matched no packages") { //NOT: warning: "github.com/skycoin/blog/..." matched no packages
			packs := strings.Split(packsStr, "\n")
			cmd.Verbose(true)
			for _, pack := range packs {
				if pack == "" {
					continue
				}
				fmt.Printf("pack: %v", pack) //TODO remove
				out, err = cmd.Run("go", "test", pack)
				outRepo.Terminal += out
				if err != nil {
					outRepo.Status = FAILED
					log.Printf("error: %v", err)
					continue
				}

				out, err = cmd.Run("go", "build", pack)
				outRepo.Terminal += out
				if err != nil {
					outRepo.Status = FAILED
					log.Printf("error: %v", err)
					continue
				}

				out, err = cmd.Run("golint", pack)
				outRepo.Terminal += out
				if err != nil {
					outRepo.Status = FAILED
					log.Printf("error: %v", err)
					continue
				}
			} // loop by packages
		} //packages list exist an need to be processed
		outRes.Repos = append(outRes.Repos, outRepo)
	} // loop by repos

	// Save to file
	js, err := json.Marshal(outRes)
	if err != nil {
		log.Printf("error: %v", err)
	}

	err = ioutil.WriteFile("output.json", js, 0644)
	if err != nil {
		log.Printf("error: %v", err)
	}
}

func mainDashboard() {
	// Getting json data
	var data outJSON
	js, err := ioutil.ReadFile("output.json")
	if err != nil {
		log.Printf("error: %v", err)
	}

	err = json.Unmarshal(js, &data)
	if err != nil {
		log.Printf("error: %v", err)
	}

	// Setup Template
	tmpl, err := template.ParseFiles("dashboard_template.html")
	if err != nil {
		log.Printf("error: %v", err)
	}

	// Execute Template
	var buff bytes.Buffer
	err = tmpl.Execute(&buff, map[string]interface{}{"Data": data})
	if err != nil {
		log.Printf("error: %v", err)
	}

	// Generate html files
	now := time.Now()
	outhtmlfile := fmt.Sprintf("dashboard_%4d-%02d-%02d_%02d%02d%02d.html", now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second())
	err = ioutil.WriteFile(outhtmlfile, []byte(buff.String()), 0644)
	if err != nil {
		log.Printf("error: %v", err)
	}

}
