# ci-dashboard

Dashboard For Continuous Integration of Skycoin Project Repos

## How to use it?
Follow these steps:
1. Go to `/cmd` directory

    `cd /cmd`

2. Generate `output.json` and `dashboard.html`

    `make run`
