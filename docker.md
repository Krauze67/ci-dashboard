# Install Docker on Linux(Ubuntu)
[Official guide](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce)

## Setup the repository
- Update __apt__ package index:

`$ sudo apt-get update`

- Install packages to allow apt to use a repository over HTTPS:

`$ sudo apt-get install apt-transport-https ca-certificates curl software-properties-common`

- Add Docker’s official GPG key

`$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

Verify that you now have the key with the fingerprint 9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88, by searching for the last 8 characters of the fingerprint.

`$ sudo apt-key fingerprint 0EBFCD88`

```
pub   4096R/0EBFCD88 2017-02-22 \
Key fingerprint = 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88 \
uid                  Docker Release (CE deb) <docker@docker.com> \
sub   4096R/F273FCD8 2017-02-22
```

- Select ***stable*** repository

`$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $lsb_release -cs) stable"`

- Install the latest version of Docker CE

`$ sudo apt-get install docker-ce`

# Install Docker on MAC OSX

Download and install Docker CE for Mac: [Download](https://store.docker.com/editions/community/docker-ce-desktop-mac)

# Run Docker Instance With Golang Installed

## Build and run docker container

- Make a simple file named: __findlinks.go__
```
package main

import (
	"fmt"
	"log"
	"net/http"

	"golang.org/x/net/html"
)

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe("0.0.0.0:8000", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	url := r.URL.Query().Get("q")
	fmt.Fprintf(w, "Page = %q\n", url)
	if len(url) == 0 {
		return
	}
	page, err := parse("https://" + url)
	if err != nil {
		fmt.Printf("Error getting page %s %s\n", url, err)
		return
	}
	links := pageLinks(nil, page)
	for _, link := range links {
		fmt.Fprintf(w, "Link = %q\n", link)
	}
}

func parse(url string) (*html.Node, error) {
	fmt.Println(url)
	r, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("Cannot get page")
	}
	b, err := html.Parse(r.Body)
	if err != nil {
		return nil, fmt.Errorf("Cannot parse page")
	}
	return b, err
}

func pageLinks(links []string, n *html.Node) []string {
	if n.Type == html.ElementNode && n.Data == "a" {
		for _, a := range n.Attr {
			if a.Key == "href" {
				links = append(links, a.Val)
			}
		}
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		links = pageLinks(links, c)
	}
	return links
}
```

- Create ***Dockerfile*** to build image

```
# Select golang version to run in docker container
FROM golang:latest

# Setup workdir
WORKDIR /go/src/app

# Sync current directory to workdir
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["app"]
```

- Build docker image from ***Dockerfile***

`$docker build -t my-golang-app .`

- Run docker container from image with port 8000

`$docker run -it --rm --name my-running-app -p 8000:8000  my-golang-app`

- Go to [http://localhost:8000](http://localhost:8000) from browser

## Access to docker container

- Show containers are running
```
$docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                    NAMES
c80c99406157        my-golang-app       "app"               2 hours ago         Up 2 hours          0.0.0.0:8000->8000/tcp   my-running-app
```

- Use __docker exec__ to access a container

`$docker exec -it my-running-app bash`
